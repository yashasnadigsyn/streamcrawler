import scrapy
from bs4 import BeautifulSoup as soup

class StreamspiderSpider(scrapy.Spider):
    name = "streamspider"
    allowed_domains = ["streamblasters.fans"]
    start_urls = ["https://streamblasters.fans"]

    def __init__(self):
        self.crawled_urls = []

    def parse(self, response):
        cont = soup(response.text, "lxml")
        yield {
            "title": cont.find("title").text,
            "url": response.url,
            "content": " ".join((cont.text).split())
        }

        self.crawled_urls.append(response.url)

        links = [a["href"] for a in cont.find_all("a", href=True)]

       # yield {
        #    "urls": links
       # }

        filtered_links = [
            link
            for link in links
            if link.startswith("https://www.streamblasters.fans")
            and not link.endswith((".pdf", ".jpg", ".png", ".gif"))
            and link not in self.crawled_urls
        ]

        #yield {
         #   "filtered": filtered_links
        #}

        for link in filtered_links:
            yield response.follow(link, callback=self.parse, priority=response.request.priority - self.crawler.settings.get('DEPTH_PRIORITY'))
