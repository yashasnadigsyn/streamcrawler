# StreamCrawler

This is a simple web crawler written in python for crawling a website.

## Run locally

Clone the project

```bash
  git clone https://codeberg.org/yashasnadigsyn/streamcrawler
```

Go to the project directory

```bash
  cd my-project
```

Install requirements

```bash
  pip install scrapy beautifulsoup4
```

Before running change the `allowed_domains` and `start_urls` according to your needs

```python
allowed_domains = ["streamblasters.fans"]
start_urls = ["https://streamblasters.fans"]
```

Run it

```bash
  scrapy crawl streamspider -o urls.json
```


## License

[AGPL](https://choosealicense.com/licenses/agpl-3.0/)

